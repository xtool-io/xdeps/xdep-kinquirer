package xdep.kinquirer.components

import xdep.kinquirer.KInquirer
import xdep.kinquirer.core.KInquirerEvent.*
import org.fusesource.jansi.Ansi.*
import xdep.kinquirer.core.*
import xdep.kinquirer.core.toAnsi
import xdep.kinquirer.factory.preferencesFactory
import java.math.BigDecimal
import java.nio.file.Paths

internal class InputComponent(
    val message: String,
    val default: String = "",
    val hint: String = "",
    val history: History? = null,
    val validation: (s: String) -> ValidationContext = { ValidationContext(true, "") },
    val filter: (s: String) -> Boolean = { true },
    val transform: (s: String) -> String = { s -> s },
) : Component<String> {
    private var value: String? = null
    private var interacting = true
    private var errorMessage = ""

    override fun value(): String = value ?: default

    override fun isInteracting(): Boolean = interacting

    override fun onEvent(event: KInquirerEvent) {
        errorMessage = ""
        when (event) {
            is KeyPressEnter -> {
                val validationContext = validation(value())
                if (validationContext.exp) {
                    interacting = false
                } else {
                    errorMessage = validationContext.failureMessage
                }
            }

            is KeyPressBackspace -> {
                value = value?.dropLast(1)
            }

            is KeyPressSpace -> {
                if (filter(" ")) {
                    value = value?.plus(" ") ?: " "
                }
            }

            is Character -> {
                val tempVal = value.orEmpty() + event.c
                if (filter(tempVal)) {
                    value = tempVal
                }
            }

            else -> {}
        }
    }

    override fun render(): String = buildString {
        // Question mark character
        append("?".toAnsi { fgGreen(); bold() })
        append(" ")

        // Message
        append(message.toAnsi { bold() })
        append(" ")

        when {
            interacting && value.isNullOrEmpty() && hint.isNotBlank() -> {
                // Hint
                append("  ")
                append(hint.toAnsi { fgBrightBlack() })
                append(ansi().cursorLeft(hint.length + 2))
            }

            interacting -> {
                // User Input
                append(transform(value()))
                // Error message
                if (errorMessage.isNotBlank()) {
                    append("  ")
                    append(errorMessage.toAnsi { bold(); fgRed() })
                    append(ansi().cursorLeft(errorMessage.length + 2))
                }
            }

            else -> {
                // User Input with new line
                appendLine(transform(value()).toAnsi { fgCyan(); bold(); })
            }
        }
    }
}


public fun KInquirer.promptInput(
    message: String,
    default: String = "",
    hint: String = "",
    history: History? = null,
    validation: (s: String) -> ValidationContext = { ValidationContext(true, "") },
    filter: (s: String) -> Boolean = { true },
    transform: (s: String) -> String = { it }
): String {
    return prompt(InputComponent(message, default, hint, history, validation, filter, transform))
}

public fun KInquirer.promptInputPassword(
    message: String,
    default: String = "",
    hint: String = "",
    mask: String = "*"
): String {
    val validation: (s: String) -> ValidationContext = { ValidationContext(true, "") }
    val filter: (s: String) -> Boolean = { true }
    val transform: (s: String) -> String = { it.map { mask }.joinToString("") }

    return prompt(InputComponent(message, default, hint, null, validation, filter, transform))
}

public fun KInquirer.promptInputNumber(
    message: String,
    default: String = "",
    hint: String = "",
    history: History? = null,
    transform: (s: String) -> String = { it }
): BigDecimal {
    val validation: (s: String) -> ValidationContext = { ValidationContext(it.matches("\\d+.?\\d*".toRegex()), "") }
    val filter: (s: String) -> Boolean = { it.matches("\\d*\\.?\\d*".toRegex()) }

    return BigDecimal(prompt(InputComponent(message, default, hint, history, validation, filter, transform)))
}
