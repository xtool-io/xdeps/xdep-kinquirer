package xdep.kinquirer

import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Component

@Component
@ComponentScan
class XdepKinquirer {
}
