package xdep.kinquirer.components

import xdep.kinquirer.core.Component
import xdep.kinquirer.core.KInquirerEvent


internal fun <T> Component<T>.onEventSequence(func: MutableList<KInquirerEvent>.() -> Unit) {
    val events = mutableListOf<KInquirerEvent>()
    events.func()
    events.forEach { event -> onEvent(event) }
}
